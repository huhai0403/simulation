import Vue from 'vue'
import App from './App.vue'
import 'lib-flexible/flexible.js'
import 'normalize.css/normalize.css'
import './assets/css/main.css'
import Vant from 'vant'
import 'vant/lib/index.css'
import 'weui'
import weui from 'weui.js'
import * as echarts from "echarts"
import router from './router/router'
Vue.config.productionTip = false
Vue.prototype.$echarts = echarts
Vue.prototype.$weui = weui
Vue.use(weui)
Vue.use(Vant)
new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
