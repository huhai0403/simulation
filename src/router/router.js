import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const routerData = [
    {
        name: 'index',
        path: '/',
        component: () => import('@/views/index.vue')
    },
    {
        name: 'one',
        path: '/one',
        component: () => import('@/views/onePage.vue')
    },
    {
        name: 'two',
        path: '/two',
        component: () => import('@/views/twoPage.vue')
    },
    {
        name: 'three',
        path: '/three',
        component: () => import('@/views/threePage.vue')
    },
    {
        name: 'four',
        path: '/four',
        component: () => import('@/views/fourPage.vue')
    },
    {
        name: 'five',
        path: '/five',
        component: () => import('@/views/fivePage.vue')
    },
    {
        name: 'six',
        path: '/six',
        component: () => import('@/views/sixPage1.vue')
    },
    {
        name: 'seven',
        path: '/seven',
        component: () => import('@/views/sevenPage.vue')
    },
    {
        name: 'eight',
        path: '/eight',
        component: () => import('@/views/eightPage.vue')
    },
    {
        name: 'nine',
        path: '/nine',
        component: () => import('@/views/ninePage.vue')
    }
    ,
    {
        name: 'ten',
        path: '/ten',
        component: () => import('@/views/tenPage.vue')
    }
]
const createRouter = () =>
  new Router({
    // mode: 'history', // require service support
    scrollBehavior: () => ({ y: 0 }),
    routes: routerData
  })
const router = createRouter()
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}
export { routerData }
export default router